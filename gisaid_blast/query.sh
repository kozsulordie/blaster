# bash query.sh ../amplicon_CDC_Pasteur_ok.fasta.txt gisaid_cov2020_sequences.fasta

QUERY=$1
SUBJECT=$2

../blast/bin/blastn -query $QUERY -db $SUBJECT -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore" -max_hsps 10 -max_target_seqs 10000 -out gisaid_blast.out

