# make gisaid seqids shorter

finput = '../amplicon_CDC_Pasteur_ok.txt'

fasta_dict = {}
with open(finput, 'rt') as f:
    for line in f.readlines():
        if line.startswith('>'):
            fname = line[1:-1] + '.fasta'
            fasta_dict[fname] = []
        fasta_dict[fname] += [line]

for fname, v in fasta_dict.items():
    with open(fname, 'wt') as g:
        for line in v:
            g.write(line)
